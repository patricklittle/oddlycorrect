<?php if( get_field('tasting_notes' ) ): ?>
    <div class="tasting-notes">
        <h3>Tasting Notes</h3>
        <p><?php the_field('tasting_notes');?></p>
    </div>
<?php endif; ?>

<div class="product-notes">
    <ul>
    <?php if( get_field('cup_profile' ) ): ?>
        <li><span>Cup Profile - </span> <?php the_field('cup_profile');?></li>
    <?php endif; ?>
    <?php if( get_field('country_of_origin' ) ): ?>
        <li><span>Country of Origin - </span> <?php the_field('country_of_origin');?></li>
    <?php endif; ?>
    <?php if( get_field('growing_region' ) ): ?>
        <li><span>Growning Region - </span> <?php the_field('growing_region');?></li>
    <?php endif; ?>
    <?php if( get_field('cultivars' ) ): ?>
        <li><span>Cultivars - </span> <?php the_field('cultivars');?></li>
    <?php endif; ?>
    <?php if( get_field('processing' ) ): ?>
        <li><span>Processing - </span> <?php the_field('processing');?></li>
    <?php endif; ?>
    <?php if( get_field('elevation' ) ): ?>
        <li><span>Elevation - </span> <?php the_field('elevation');?></li>
    <?php endif; ?>
    </ul>
</div>
