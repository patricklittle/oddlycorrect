<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '-', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PWJ6DSW');</script>
<!-- End Google Tag Manager -->
<link href="https://fonts.googleapis.com/css?family=Lora|Oswald:400,500" rel="stylesheet">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_url') ?>/apple-touch-icon.png?v=jw6YvWWR7g">
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url') ?>/favicon-32x32.png?v=jw6YvWWR7g" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url') ?>/favicon-16x16.png?v=jw6YvWWR7g" sizes="16x16">
<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url') ?>/favicon.ico?v=jw6YvWWR7g">
<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWJ6DSW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="hfeed site">
<header class="site-header">

    <?php

        if ( is_front_page() ) :
            $pageBackground = get_field('background_image', 'option');

            if($pageBackground): ?>

            <div class="site-background-image">
                <img src="<?php echo $pageBackground['url']; ?>">
            </div>

    <?php endif; endif; ?>

<?php
    // Hide Nav on Discount Page
    if(!is_page_template('templates/discount.php')) {?>
    <nav id="site-navigation" class="site-navigation">
        <a href="#" class="mobile-nav-button">Menu</a>
        <div id="responsive-menu"><?php wp_nav_menu(array('theme_location' => 'header', 'menu_id' => 'menu-header', 'menu_class' => 'header-navigation')); ?></div>
    </nav>
<?php }; ?>

    <div class=" container">
        <div class="site-branding">
            <h1 class="logo">
                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                    <img src="<?php echo get_bloginfo('template_url') ?>/assets/img/oddly-correct-logo-black.png" class="header-logo" title="<?php bloginfo('name'); ?>">
                </a>
            </h1>
        </div>
    </div>
</header>

<div class="wrap-main">
