<?php /* Template Name: Homepage */ ?>

<?php get_header(); ?>

<div class="homepage">
    <div class="welcome-message">
        <h1>Most mornings aren't exceptional. Your coffee can be.</h1>
        <a href="https://oddlycorrect.square.site/" class="big-button">Order for Pickup</a> <a href="/shop" class="big-button">Shop Now</a>
    </div>
    <?php

    // --- Latest News ---

    // $args = array( 'post_type' => 'post', 'posts_per_page' => 1 );
    // $loop = new WP_Query( $args );
    // while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <section class="content-grid">
        <div class="content-grid--image">
            <!-- <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php// the_post_thumbnail('feature');?>
            </a> -->
            <?php the_post_thumbnail('feature');?>
        </div>
        <div class="content-grid--article">
            <div class="article-wrap">
                <div class="content-grid--entry">
                    <!--<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1> -->
                    <!--<span class="entry-meta"><?php// the_date('F j, Y')?></span>-->
                    <?php// the_excerpt();?>
                    <h2 class="entry-title">Your guide to a better coffee experience</h2>
                    <p>There are lots of ways to live exotically every day, and the best we know how is through amazing coffee. Trust us to always have something new, exciting and delicious to brew. We are here for you with the coffee, gear, and knowledge to freak out your morning cup in the best way possible.</p>
                    <p><a href="/about">Learn more</a></p>
                </div>
            </div>
        </div>
    </section>
    <?php// endwhile; wp_reset_query(); ?>

    <div class="textCenter">
        <h3>Featured Products</h3>
    </div>

    <?php

    // check if the repeater field has rows of data
    if( have_rows('products') ):

        echo '<ul class="product-list">';

     	// loop through the rows of data
        while ( have_rows('products') ) : the_row();

            $post_object = get_sub_field('product');

            if( $post_object ):

            	// override $post
            	$post = $post_object;

                setup_postdata($post);

                wc_get_template_part( 'content', 'product' );

                wp_reset_postdata();

                endif;

        endwhile;

        echo '</ul>';

    else :

        // no rows found

    endif;
    ?>

    <div class="homepage-newsletter">
        <h3 class="newsletter-heading">Latest News from Oddly Correct</h3>
        <p>Sign up to get the latest from our world, including brewing advice, newest coffee arrivals and special offers and discounts.</p>

        <div class="homepage-newsletter-wrap">
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
                <form action="//oddlycorrect.us13.list-manage.com/subscribe/post?u=0992fc5bd06b36cef50f13168&amp;id=d43fc373de" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="form-inline">
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="mc-field-group newsletter-email">
                                <input type="email" value="" placeholder="your@email.com" name="EMAIL" class="required email" id="mce-EMAIL">
                            </div>
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0992fc5bd06b36cef50f13168_d43fc373de" tabindex="-1" value=""></div>
                            <div class="newsletter-button"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        </div>
                    </div>
                </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
        <!--End mc_embed_signup-->
        </div>
    </div>

</div>

<?php get_footer(); ?>
