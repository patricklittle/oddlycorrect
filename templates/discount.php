<?php /* Template Name: Discount */ ?>

<?php get_header(); ?>

  <div id="wrap-content" class="wrap-content">
    <div id="content" class="site-content">
      <section id="primary" class="content-area">
        <main id="main" class="site-main">

            <article id="post-<?php the_ID(); ?>" <?php post_class('section-article'); ?> role="article">
              <div class="page-content container">

                  <div class="homepage-newsletter" style="margin: 0 0 4em;">
                      <h4 class="newsletter-heading">Get 15% off your first order!</h4>

                      <p>Sign up to get the latest from our world, including brewing advice, newest coffee arrivals and special offers and discounts.</p>

                      <div class="homepage-newsletter-wrap">
                          <!-- Begin MailChimp Signup Form -->
                          <div id="mc_embed_signup">
                              <form action="https://oddlycorrect.us13.list-manage.com/subscribe/post?u=0992fc5bd06b36cef50f13168&amp;id=c9538e4708" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                  <div id="mc_embed_signup_scroll">
                                      <div class="form-inline">
                                          <div id="mce-responses" class="clear">
                                              <div class="response" id="mce-error-response" style="display:none"></div>
                                              <div class="response" id="mce-success-response" style="display:none"></div>
                                          </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                          <div class="mc-field-group newsletter-email">
                                              <input type="email" value="" placeholder="your@email.com" name="EMAIL" class="required email" id="mce-EMAIL">
                                          </div>
                                          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0992fc5bd06b36cef50f13168_c9538e4708" tabindex="-1" value=""></div>
                                          <div class="newsletter-button"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                      <!--End mc_embed_signup-->
                      </div>
                  </div>
              </div>
            </article>
        </main>
      </section>
    </div>
  </div>
<?php // get_sidebar(); ?>
<?php// get_footer(); ?>
