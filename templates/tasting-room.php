<?php /* Template Name: Tasting Room */ ?>

<?php get_header(); ?>

  <div id="wrap-content" class="wrap-content">
    <div id="content" class="site-content">
      <section id="primary" class="content-area">
        <main id="main" class="site-main">
        <?php if ( have_posts() ) {
          while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('section-article'); ?> role="article">
              <header class="page-header <?php if(has_post_thumbnail()) { echo 'has-featured-image'; } ?>">
                  <div class="page-title">
                      <h1><?php the_title(); ?></h1>
                      <div class="tasting-room-hours">
                          <p>3940 Main St.<br> 
                          Kansas City, MO 64111</p>
                          <p><?php the_field('hours') ?></p>
                      </div>
                  </div>
                 
                 <?php if(has_post_thumbnail()): ?>
                      <div class="page-featured-image">
                          <?php the_post_thumbnail();?>
                      </div>
                  <?php endif; ?>
                  
              </header>

              <div class="page-content container">
                  
                  <?php the_content(); ?>
                 
                  <?php if( have_rows('pour_over_offerings') ): ?>
                   	<h3>Brew Bar</h3>
                    <table class="table-menu">
                         <?php while ( have_rows('pour_over_offerings') ) : the_row(); ?>
                             <tr>
                               <td>
                                   <h4><?php the_sub_field('coffee_name'); ?></h4>
                                   <p><?php the_sub_field('coffee_description'); ?></p>
                               </td>
                               <td class="coffee-price"><?php the_sub_field('coffee_price'); ?></td>
                             </tr>
                        <?php endwhile; ?>
                    </table>
                    <small>Our coffee offerings are served without milk or sugar.</small>
                  <?php endif; ?>
                  <?php wp_link_pages(); ?>
              </div>
            </article>
          <?php endwhile;
        } else { ?>
          <article id="post-0" class="post no-results not-found">
            <header class="entry-header">
              <h1><?php _e( 'Not found', 'oddlycorrect' ); ?></h1>
            </header>
            <div class="entry-content">
              <p><?php _e( 'Sorry, but your request could not be completed.', 'oddlycorrect' ); ?></p>
              <?php get_search_form(); ?>
            </div>
          </article>
        <?php } ?>
        </main>
      </section>
    </div>
  </div>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>
