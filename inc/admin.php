<?php

add_editor_style( 'admin-style.css' );

// Add Google fonts to editor
add_action( 'after_setup_theme', 'oddlycorrect_editor_fonts' );
function oddlycorrect_editor_fonts() {
	$font_url = str_replace( ',', '%2C', '//fonts.googleapis.com/css?family=Asar|Oswald:400,500' );
	add_editor_style( $font_url );
}
