<?php

// Add Woocommerce Support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Remove Breadcrumbs
remove_action('woocommerce_before_main_content','woocommerce_breadcrumb', 20);

// Remove Add to Cart Button from the Product Loop
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

// Add Class to Product Thumbnail in product list
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
    function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0 ) {
		global $post;
		$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );
		if ( has_post_thumbnail() ) {

			$props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
			return get_the_post_thumbnail( $post->ID, $image_size, array(
				'title'	 => $props['title'],
				'alt'    => $props['alt'],
                'class'  => 'product-image'
			) );
		} elseif ( wc_placeholder_img_src() ) {
			return wc_placeholder_img( $image_size );
		}
    }
}

// Setup Single Product Page
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
// remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
// remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
// remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

// Remove Tabs from Product Page
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

// Add Product Description
function woocommerce_template_product_description() {
  woocommerce_get_template( 'single-product/tabs/description.php' );
  include( get_template_directory() . '/woocommerce/single-product/product-meta.php' );
  woocommerce_get_template( 'single-product/price.php' );
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );


function hook_open_div_product_wrap() {
    echo '<div class="product-wrap">';
}

function hook_open_div_clearfix() {
    echo '<div class="clearfix">';
}

function hook_open_div_product_footer() {
    echo '<div class="product-footer">';
}

function hook_close_div() {
    echo '</div>';
}

// Archive product wrap
add_action('woocommerce_before_shop_loop_item', 'hook_open_div_product_wrap', 0);
add_action('woocommerce_after_shop_loop_item', 'hook_close_div', 99);

// Single Product Page
add_action('woocommerce_before_single_product_summary', 'hook_open_div_clearfix', 0);
add_action('woocommerce_after_single_product_summary', 'hook_close_div', 0);
add_action('woocommerce_after_single_product_summary', 'hook_open_div_product_footer', 5);
add_action('woocommerce_after_single_product_summary', 'hook_close_div', 90);
