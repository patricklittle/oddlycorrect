
      </div> <!-- .wrap-main -->
      <div class="site-footer">
        <footer class="container">
            <div class="footer-navigation">
              <nav class="footer-navigation-column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-1', 'menu_id' => 'menu-footer-1', 'menu_class' => 'footer-menu' ) ); ?>
              </nav>
              <nav class="footer-navigation-column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-2', 'menu_id' => 'menu-footer-2', 'menu_class' => 'footer-menu' ) ); ?>
              </nav>
              <nav class="footer-navigation-column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-3', 'menu_id' => 'menu-footer-3', 'menu_class' => 'footer-menu' ) ); ?>
              </nav>
              <nav class="footer-navigation-column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-4', 'menu_id' => 'menu-footer-4', 'menu_class' => 'footer-menu' ) ); ?>
              </nav>
          </div>
        </footer>
        <div class="copyright">Copyright © 2017 Oddly Correct All Rights Reserved.</div>
      </div>
    </div>
  <?php wp_footer(); ?>
  
  <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-97850604-1', 'auto');
      ga('send', 'pageview');

    </script>
  </body>
</html>
