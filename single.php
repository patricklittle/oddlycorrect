<?php get_header(); ?>

  <div id="wrap-content" class="wrap-content">
    <div id="content" class="site-content">
      <section id="primary" class="content-area">
        <main id="main" class="site-main">
        <?php if ( have_posts() ) {
          while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('section-article'); ?> role="article">
              <header class="page-header">
                  <h1 class="page-title post-title"><?php the_title(); ?></h1>

                 <?php if(has_post_thumbnail()): ?>
                      <div class="post-featured-image">
                          <?php the_post_thumbnail();?>
                      </div>
                  <?php endif; ?>

              </header>

              <div class="page-content container">
                <?php the_content(); ?>
                <?php wp_link_pages(); ?>
              </div>
            </article>
          <?php endwhile;
        } else { ?>
          <article id="post-0" class="post no-results not-found">
            <header class="entry-header">
              <h1><?php _e( 'Not found', 'oddlycorrect' ); ?></h1>
            </header>
            <div class="entry-content">
              <p><?php _e( 'Sorry, but your request could not be completed.', 'oddlycorrect' ); ?></p>
              <?php get_search_form(); ?>
            </div>
          </article>
        <?php } ?>
        </main>
      </section>
    </div>
  </div>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>
