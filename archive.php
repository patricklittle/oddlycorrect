<?php get_header(); ?>
  <div id="wrap-content" class="wrap-content">
    <div id="content" class="site-content">
      <section id="primary" class="content-area page-content">
        <main id="main" class="site-main">

        <?php if (have_posts()) { while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class('section-article'); ?> role="article">

              <header class="entry-header">
                  <h2><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
              </header>

              <footer class="entry-meta">
                  <h5>Posted on <?php the_date('F j, Y') ?></h5>
              </footer>

              <div class="entry-content">
                  <?php the_content(); ?>
                  <?php wp_link_pages(); ?>
              </div>

            </article>

          <?php endwhile; } else { ?>

          <article id="post-0" class="post no-results not-found">
            <header class="entry-header">
              <h3><?php _e('Not found', 'oddlycorrect'); ?></h3>
            </header>
            <div class="entry-content">
              <p><?php _e('Sorry, but your request could not be completed.', 'oddlycorrect'); ?></p>
              <?php get_search_form(); ?>
            </div>
          </article>

        <?php } ?>

        </main>
      </section>
    </div>
  </div>
<?php // get_sidebar();?>
<?php get_footer(); ?>
