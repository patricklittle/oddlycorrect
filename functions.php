<?php // ==== FUNCTIONS ==== //

// Load configuration defaults for this theme; anything not set in the custom configuration (above) will be set here
defined( 'oddlycorrect_SCRIPTS_PAGELOAD' )       || define( 'oddlycorrect_SCRIPTS_PAGELOAD', true );

// An example of how to manage loading front-end assets (scripts, styles, and fonts)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/assets.php' );

// Required to demonstrate WP AJAX Page Loader (as WordPress doesn't ship with even simple post navigation functions)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/navigation.php' );

// Only the bare minimum to get the theme up and running
function oddlycorrect_setup() {

  // HTML5 support; mainly here to get rid of some nasty default styling that WordPress used to inject
  add_theme_support( 'html5', array( 'search-form', 'gallery' ) );

  // Automatic feed links
  add_theme_support( 'automatic-feed-links' );

  add_theme_support( 'title-tag' );

  // $content_width limits the size of the largest image size available via the media uploader
  // It should be set once and left alone apart from that; don't do anything fancy with it; it is part of WordPress core
  global $content_width;
  $content_width = 960;

  // Register header and footer menus
  register_nav_menu( 'header', __( 'Header menu', 'oddlycorrect' ) );
  register_nav_menu( 'footer-1', __( 'Footer Menu 1', 'oddlycorrect' ) );
  register_nav_menu( 'footer-2', __( 'Footer Menu 2', 'oddlycorrect' ) );
  register_nav_menu( 'footer-3', __( 'Footer Menu 3', 'oddlycorrect' ) );
  register_nav_menu( 'footer-4', __( 'Footer Menu 4', 'oddlycorrect' ) );

}
add_action( 'after_setup_theme', 'oddlycorrect_setup', 11 );

// Sidebar declaration
function oddlycorrect_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Main sidebar', 'oddlycorrect' ),
    'id'            => 'sidebar-main',
    'description'   => __( 'Appears to the right side of most posts and pages.', 'oddlycorrect' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ) );
}
add_action( 'widgets_init', 'oddlycorrect_widgets_init' );

add_theme_support( 'post-thumbnails' );

function wpdocs_excerpt_more( $more ) {
    return sprintf( '...<a class="read-more" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'Read More', 'oddlycorrect' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


// Custom Post Types
include_once('inc/custom-post-types.php');

// Custom Woocommerce Support
include_once('inc/custom-woocommerce.php');

// Custom Images
include_once('inc/image-sizes.php');

// Options Page
include_once('inc/options-page.php');

// Admin styles
include_once('inc/admin.php');
